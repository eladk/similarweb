## SimilarWeb - Home Test

I made the test on nodejs server.

run the command `npm install`

### run the load balancer + clients : 
-   server.js - creating a 7 server with different port. to run the servers 
run the command `node server.js`. you will see log in the console the ports
- ./lb/lb.js - the load balance. run the command node lb/lb.js
 
### config file
see config.yml include the application configuration
 - `servers` - server list 
 - `retrying_interval` - retrying interval when getting error on post calls
 
### monitoring
In log file i monitor: 
- `request` - all income load balance request
- `errror-http` - save the post request when status is not 201
- `success-http` - save when server return valid response
- `uptime` - should run it on cronjob or `node lb/ping.js ` create a http request to all server to check if server down
 
 
### servers
- Etch post call i added a random timeout response and etch X call the server will return 503 to make a call again from load balancer


###Highlights: 
- To make a call for testing i export postman collection `similarweb.postman_collection`
- To test the response time attached jmiter file with 100 call in the sime time to check the server response time 
