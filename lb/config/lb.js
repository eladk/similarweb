const _yaml   = require('js-yaml'),
      _fs     = require('fs')
    ;
/**
 *  @type {Array} - loading yml file and convert to object
 */
const config = _yaml.safeLoad(_fs.readFileSync(`${ global._basePath}/config/config.yml`, 'utf8'));

/**
 *
 * @returns {Array} - List of post servers
 */
exports.getServerList = function get() {
    return config.servers;
};

/**
 * @returns {int} - Interval to next call http
 */
exports.getRetryingInterval = function get() {
    return config.retrying_interval;
};

/**
 * @returns {int} - Interval to next call http
 */
exports.getApplicationLogLevel = function get() {
    return config.logLevel;
};
/**
 * @returns {int} - Interval to next call http
 */
exports.getExponentialRetrying = function get() {
    return config.exponential_retrying;
};
