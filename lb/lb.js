const bodyParser = require('body-parser'),
      express    = require('express'),
      app        = express(),
      yaml       = require('js-yaml'),
      fs         = require('fs'),
      path       = require('path')
    ;

global._basePath = path.join(__dirname);

const _request =  require(path.join(global._basePath, 'request/http/base'));
const _monitor =  require(path.join(global._basePath, 'utils/monitor'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


/**
 * Catch all requests. post, get, put, etc...
 * Catch all get params
 * Catch all post params
 */
app.all('/*', (req, res, next) => {

    _monitor.add(_monitor.TYPE_REQUEST, [req.method,req.originalUrl ]);

    const params = (req.method === 'POST') ? req.body : req.query ;
    _request.call(req.originalUrl, req.method,req.headers, params).then((response) => {
        console.log('response', response);

        res.send(response)
    });
});

process.on('uncaughtException', function(err) {

});


app.listen(8080, function () {
    console.log('load balance listening on port 8080')
});
