const _path      = require('path');
global._basePath = _path.join(__dirname);
const _config    = require(`${global._basePath}/config/lb`);
const _monitor   = require(`${global._basePath}/utils/monitor`);
const _request   = require('request');

/**
 * Go over on all server under balance and create a request to status toe check id the server up
 */
_config.getServerList().forEach(function (server) {


    _request.get(`${server}/status`, {}, (error, response, body) => {

        _monitor.add(_monitor.UP_TIME, [server, response.statusCode])

    });


});
