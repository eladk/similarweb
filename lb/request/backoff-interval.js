const config      = require(`${global._basePath}/config/lb`);
const exponentialRetrying = config.getExponentialRetrying();

/**
 * calculate a new interval for next request
 * @param pastRequestInterval
 * @returns {*}
 */
exports.nextInterval = function get(pastRequestInterval) {

    newInerval = Math.round(exponentialRetrying.minValue  * exponentialRetrying.multiplier * (1 + Math.random() * exponentialRetrying.randomisationFactor));

    return pastRequestInterval + newInerval;
};