const _request     = require('request');
const config      = require(`${global._basePath}/config/lb`);
const countServers = config.getServerList().length;
const _monitor     = require(`${global._basePath}/utils/monitor`);
const backOffInterval     = require('../backoff-interval');
var roundServer    = 0;

/**
 * Getting the next server to make a get call
 * @returns {String} - server url
 * @private
 */
function _getNextServer() {
    if (roundServer === countServers) {
        roundServer = 0;
    }
    serverUrl = config.getServerList()[roundServer];
    roundServer += 1;
    return serverUrl;
}

/**
 * Execute post http request
 * @param {String} url - request url
 * @param {Object} headers - request params
 * @param {Object} params - request params
 * @returns {Promise} - return resolved when http status is 201 otherwise return reject
 */
function post(url,headers, params) {
    return new Promise((resolve, reject) => {

        _request.post({url: url, form: params, headers: headers }, (error, response, body) => {

            if (response.statusCode === 201 || response.statusCode === 200) {
                resolve(body, response.statusCode);
            } else {

                reject(response.statusCode);
            }
        });
    });


}

/**
 * Execute get http request
 * @param {String} url - request url
 * @param {Object} headers - request params
 * @param {Object} params - request params
 * @returns {Promise} - return resolved when http status is 201 otherwise return reject
 */
function getRequest(url,headers, params) {

    console.log(headers);
    return new Promise((resolve, reject) => {
        _request.get({url: url, qs: params, headers: headers}, (error, response, body) => {

            const statusCode = response.statusCode;
            resolve({ body, statusCode });

        });
    });
}

/**
 * Wrapper http request
 * @param {String} uri - url action
 * @param {string} type - http type get, post
 * @param {Object} headers - http headers
 * @param {Object} params - list of request params
 * @returns {Promise}
 */
exports.call = function get(uri, type, headers, params) {

    return new Promise((resolve, reject) => {

        switch (type) {
            case 'GET':
                // call to _getNextServer function for getting the nex server url
                const _url = `${_getNextServer()}${uri}`;
                getRequest(_url,headers, params).then((response) => {

                    if (response.statusCode === 200) {
                        _monitor.add(_monitor.HTTP_SUCCESS, [type, _url, response.statusCode]);
                    } else {
                        _monitor.add(_monitor.HTTP_ERRORS, [type, _url, response.statusCode]);

                    }
                    resolve(response.body, response.statusCode);
                }, (e) => {
                    reject();

                });

                break;

            default:

                /**
                 * Count of successfully server response. counter++ when the status code
                 * is 201, otherwise execute again the call
                 * @type {number}
                 * @private
                 */
                var _countSuccessResponse = 0;
                // forEach on server list
                config.getServerList().forEach(function (server) {
                    // private self function
                    _nextIntervalRequest = 0;
                    function _makePostCall(url) {
                        _nextIntervalRequest = backOffInterval.nextInterval(_nextIntervalRequest);
                        var start = new Date();
                        post(url,headers, params).then((body, statusCode) => {
                            var msResponseTime = new Date() - start;
                            
                            resolve(body, statusCode);
                            _countSuccessResponse += 1;
                            _monitor.add(_monitor.HTTP_SUCCESS, [type.toUpperCase(), url,  msResponseTime]);

                        }, (statusCode) => {
                            // on error write status to log file an tring to make again the call after X RetryingInterval configuration
                            _monitor.add(_monitor.HTTP_ERRORS, [type.toUpperCase(), url, statusCode]);
                            // if status code not 201 the promiss will return reject and call again until the status is 201.
                            setTimeout(function () {
                                _makePostCall(`${server}${uri}`);
                            }, _nextIntervalRequest);
                        });
                    }
                   
                    _makePostCall(`${server}${uri}`);
                });
        }

    });

};