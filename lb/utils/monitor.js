const fs = require('fs');
const moment = require('moment');
const applicationLogLevel      = require(`${global._basePath}/config/lb`).getApplicationLogLevel();
const _logLevelEnum = {
    'VERBOSE' : 0,
    'DEBUG' : 1,
    'INFO' : 2,
    'WARN' : 3,
    'ERROR' : 4
};

exports.LOG_VERBOSE = _logLevelEnum.VERBOSE;
exports.LOG_DEBUG = _logLevelEnum.DEBUG;
exports.LOG_INFO = _logLevelEnum.INFO;
exports.LOG_WARN = _logLevelEnum.WARN;
exports.LOG_ERROR = _logLevelEnum.ERROR;



/**
 * @type {string} - Path to save the log files
 */
const monitorPathFolder = `${global._basePath}/log`;

/**
 * @type {string} - request file - all incoming http request
 */
exports.TYPE_REQUEST = 'requests';

/**
 * @type {string} - error-http file - all http errors
 */
exports.HTTP_ERRORS = 'error-http';

/**
 * @type {string} - success-http file - all success http calls
 */
exports.HTTP_SUCCESS = 'success-http';

/**
 * @type {string} - uptime file - check the status of the servers
 */
exports.UP_TIME = 'uptime';

/**
 * Write to file
 * @param {String} type - type of the file
 * @param {Array} date - data to save
 * @param {String} logLevel - type of log level, on default LOG_ERROR

 */
exports.add = function get(type, date, logLevel) {

    logLevel = logLevel || this.LOG_INFO;

    if(parseInt(logLevel) >= parseInt(_logLevelEnum[applicationLogLevel]))
    {
        var currentDate = moment().format('Y-m-d H:m:s');

        fs.appendFile(`${monitorPathFolder}/${type}.log`, `${currentDate},${date.join()}\r\n`, function (err) {
            if (err) throw err;
        });
        
    }



};