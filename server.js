var express = require('express'),
    bodyParser = require('body-parser');

const listen = function (port) {
    return new Promise(function (resolve, reject) {
        var app    = express();
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json());
        function getRandomArbitrary(min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }


        var server = app.listen(port, function () {
            console.log('app listening on port ' + port);

            /**
             * POST: register
             */
            app.post('/register', function (req, res) {

                const timeOutResponseTime = getRandomArbitrary(0,800);
                const statusReturn = (Math.floor(Math.random() * 5) % 2) ? 201 : 503;

                setTimeout(function () {
                    res.status(statusReturn).send({
                        port: `${port} - /register`
                    });
                }, timeOutResponseTime);

            });


            /**
             * POST: changePassword
             */
            app.post('/changePassword', function (req, res) {
                const timeOutResponseTime = getRandomArbitrary(0,800);
                const statusReturn = (Math.floor(Math.random() * 5) % 2) ? 201 : 503;

                setTimeout(function () {
                    res.status(statusReturn).send({
                        port: `${port} - /changePassword`
                    });
                }, timeOutResponseTime);

            });

            /**
             * GET: Login
             */
            app.get('/login', function (req, res) {

                res.send({
                    port: `${port} - /login`
                }).status(200);
            });


            /**
             * GET: Check server status
             */
            app.get('/status', function (req, res) {
                res.send({}).status(200);
            });

            resolve(server);
        });
    })
};

/**
 * Upload a server listeners
 */
listen(3000);
listen(3001);
listen(3002);
listen(3003);
listen(3004);
listen(3005);
listen(3006);